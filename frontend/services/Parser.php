<?php
/**
 * Created by PhpStorm.
 * User: karim
 * Date: 14.05.18
 * Time: 13:15
 */

namespace frontend\services;

class Parser
{
    public function parse($html)
    {
        $dom = new \DOMDocument();
        @$dom->loadHTML($html);
        $x = new \DOMXPath($dom);

        $balance = 0;
        $data = [];
        foreach ($x->query("//tr") as $node) {
            if ($node->childNodes->item(2) !== null) {
                $type = $node->childNodes->item(2)->textContent;
                switch ($type) {
                    case 'balance':
                    case 'buy':
                        $openTime = $node->childNodes->item(1)->textContent;
                        $value = floatval($node->lastChild->textContent);
                        $balance += $value;
                        $data[] = [
                            'openTime' => $openTime,
                            'balance' => round($balance, 2), // json_encode float округляет с большей точностью
                        ];
                        break;
                    case 'buy stop':
                        $openTime = $node->childNodes->item(1)->textContent;
                        $data[] = [
                            'openTime' => $openTime,
                            'balance' => round($balance, 2),
                        ];
                }
            }
        }
        return $data;
    }
}