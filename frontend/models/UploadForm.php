<?php
/**
 * Created by PhpStorm.
 * User: karim
 * Date: 14.05.18
 * Time: 16:07
 */

namespace frontend\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'html'],
        ];
    }

    public function data()
    {
        if (!$this->validate()) {
            return false;
        }
        return file_get_contents($this->file->tempName);
    }
}