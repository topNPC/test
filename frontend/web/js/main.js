var ctx = document.getElementById("myChart");
var data = JSON.parse(ctx.getAttribute('data-data'));
var keys = data.map(a => a.openTime);
var values = data.map(a => a.balance);
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: keys,
        datasets: [{
            label: 'Balance',
            data: values,
            fill: false,
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        elements: {point: {radius: 1}},
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: false
                }
            }]
        }
    }
});