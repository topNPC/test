<?php

/* @var $this yii\web\View */

use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <button>Submit</button>

    <?php ActiveForm::end() ?>

    <canvas id="myChart" width="400" height="400" data-data='<?= $data ?>'></canvas>
</div>
